//
//  SecondaryColorCollectionViewCell.swift
//  Grid NWO
//
//  Created by Sanchit Goel on 05/01/18.
//  Copyright © 2018 Sanchit Goel. All rights reserved.
//

import UIKit

class SecondaryColorCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var boundaryView: UIView!
  @IBOutlet weak var coloredView: UIView!
  
}
