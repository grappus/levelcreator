//
//  GridView.swift
//  Grid NWO
//
//  Created by Sanchit Goel on 09/10/17.
//  Copyright © 2017 Sanchit Goel. All rights reserved.
//

import UIKit

class GridView: UIView {

  var initialPoint = CGPoint(x: 0, y: 0)
  var finalPoint = CGPoint(x: 0, y: 0)
  var horizontal:Bool?
  
  init(frame: CGRect, initialPoint:CGPoint, finalPoint: CGPoint, horizontal: Bool) {
    super.init(frame: frame)
    backgroundColor = UIColor.black
    self.initialPoint = initialPoint
    self.finalPoint = finalPoint
    self.horizontal = horizontal
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: initialPoint.x, y: initialPoint.y))
    path.addLine(to: CGPoint(x: finalPoint.x, y:initialPoint.y))
    path.close()
//    path.lineWidth = 2
//    UIColor.black.set()
//    path.stroke()
  }
  
}
