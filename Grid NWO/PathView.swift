//
//  PathView.swift
//  Grid NWO
//
//  Created by Sanchit Goel on 02/01/18.
//  Copyright © 2018 Sanchit Goel. All rights reserved.
//

import UIKit

class PathView: UIView {
  
  var color: UIColor?
  var numberOfBlocks: Int?
  var orientation: String?
  var leadingBlocks: CGFloat = 0
  var verticalBlocks: CGFloat = 0
  var selectedColorIndex: IndexPath?
  
  init(frame: CGRect, color: UIColor, orientation: String, number: Int, indexpath: IndexPath) {
    super.init(frame: frame)
    backgroundColor = color
    self.numberOfBlocks = number
    self.color = color
    self.orientation = orientation
    self.selectedColorIndex = indexpath
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
